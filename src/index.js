export * as WeiboAPI from './weibo';
export * as QQAPI from './qq';
export * as WechatAPI from './wechat';
export * as AlipayAPI from './alipay';
